 <!doctype html>

<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta name="author" content="Sumon Rahman">
    <meta name="description" content="">
    <meta name="keywords" content="HTML,CSS,XML,JavaScript">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Title -->
    <title>Maxing The Connection</title>
    <!-- Place favicon.ico in the root directory -->
    <link rel="apple-touch-icon" href="../images/paw.png">
    <link rel="shortcut icon" type="image/ico" href="../images/paw.png" />
    <!-- Plugin-CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/linearicons.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/animate.css">
    <!-- Main-Stylesheets -->
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/responsive.css">
    <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    <!--[if lt IE 9]>
        <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body data-spy="scroll" data-target=".mainmenu-area">
    <!-- Preloader-content -->
    <div class="preloader">
        <span><i class="lnr lnr-sun"></i></span>
    </div>
    <!-- MainMenu-Area -->
    <nav class="mainmenu-area" data-spy="affix" data-offset-top="200">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#primary_menu">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#"><img src="../images/paw.png" alt="Logo"></a>
            </div>
            <div class="collapse navbar-collapse" id="primary_menu">
                @guest 
                <ul class="nav navbar-nav mainmenu">

                    <li class="active"><a href="{{ url('/') }}">Home</a></li>
                </ul>
                @else
                <ul class="nav navbar-nav mainmenu">
                    <li class="active"><a href="{{ url('/home') }}">Profile</a></li>
                    <li class="active"><a href="{{ url('/notifcation') }}">Notifcation</a></li>
                    <li><a href="{{ url('create') }}">Add Tweet</a></li>
                    <li class="active"><a href="{{ url('/messages') }}">Messages</a></li>
                </ul>
                @endguest
                  <div class="links">
    <div class="cl">&nbsp;</div>
    @guest
  
    <a href="{{ url('/login') }}" class="btn btn-primary" type="submit">Sign In</a> 
    <a href="{{ url('/register') }}" class="btn btn-primary" type="submit">Create account</a>
    @else
   
    <a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('frm-logout').submit();">
      
    Logout
    <form id="frm-logout" action="{{ url('/logout') }}" method="POST" style="display: none;">
    @csrf
    </form>

    @endguest
               
                </div>
            </div>
        </div>
    </nav>

<body data-spy="scroll" data-target=".mainmenu-area">
    <!-- Preloader-content -->
    <div class="preloader">
        <span><i class="lnr lnr-sun"></i></span>
    </div>
<div class="navbar-header">
        <a class="navbar-brand" href="#" style=" display: block;
  margin-left: 900px;
  margin-right: auto;
  margin-top: 100px;
  width: 100%;"><img src="../images/dogstalking.png" alt="Logo"></a>

          </div>
 <header class="home-area overlay" id="home_page">
        <div class="container">
            <div class="row">
                
                 
                
                </div>
                <div class="col-xs-12 col-md-7">
              
                    <div class="space-80 hidden-xs"></div>
                    <h1 class="wow fadeInUp" data-wow-delay="0.4s" style="font-size: 80px">Maxing The Connection.</h1>
                    <div class="space-20"></div>
                    <div class="desc wow fadeInUp" data-wow-delay="0.6s">
                  
                    </div>

              
            </div>
        </div>
    </header>