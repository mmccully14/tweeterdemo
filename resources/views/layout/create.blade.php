
@section('layout.header')




<div class="fh5co-about-animate-box">
	<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
		<h2> Create Post</h2><br>
	</div>

		<div class="container" style="margin-bottom: 18px;">
			<div class="col-md-8 col-md-offset-2 aminate-box">
				<div class="row">

		<form  action="{{ url('/create') }}" method="post">
		@csrf
		<div class="col-md-12">
			 <div class="form-group row">
                            <label for="title" class="col-sm-4 col-form-label text-md-right">{{ __('Title') }}</label>

                            <div class="col-md-6">
                                <input id="title" type="text" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" value="{{ old('title') }}" required autofocus>

                                @if ($errors->has('title'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="body" class="col-md-4 col-form-label text-md-right">{{ __('body') }}</label>

                            <div class="col-md-6">
                                <input id="body" type="body" class="form-control{{ $errors->has('body') ? ' is-invalid' : '' }}" name="body" required>

                                @if ($errors->has('body'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('body') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


			    <button type="submit" class="btn btn-primary">

                    {{ __('Create Post') }}
                                </button>
            @csrf
	</div>
		</form>

		    </div>

		</div>
</div>
@endsection