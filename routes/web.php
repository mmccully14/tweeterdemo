<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/','PostsController@index');


Auth::routes();

Route::get('/home', 'PostsController@home');



Route::get('/post/{id}', 'PostsController@show');
Route::get('/post/{id}/edit', 'PostsController@edit');
Route::get('/post', 'PostsController@home');
Route::post('/post/{id}', 'PostsController@update');
Route::post('/post', 'PostsController@home');
Route::post('/create', 'PostsController@create');


Route::get('/newpost', 'PostsController@edit');
Route::post('/comment', 'PostsController@storeComment');


Route::get('/admin', 'AdminController@index')->middleware('authenticated', 'admin');
Route::get('admin_area', ['middleware' => 'admin', function(){

}]);


Route::get('/users', 'AdminController@index')->middleware('authenticated', 'admin');
Route::get('/user/{id}/edit', 'AdminController@edit');
Route::get('/users/{id}/delete', 'AdminController@delete')->middleware('admin');
Route::get('/users', 'AdminController@update')->middleware('authenticated', 'admin');




